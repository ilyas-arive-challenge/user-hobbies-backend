export default {
  collectCoverage: true,
  coverageProvider: "v8",
  preset: "ts-jest",
  testEnvironment: "jest-environment-node",
  testMatch: [
    "**/__tests__/**/?(*.)+(spec|test).[jt]s?(x)"
  ],
  verbose: true
};