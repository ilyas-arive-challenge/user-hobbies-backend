# User-Hobbies backend
Backend for the application User Hobbies
## Required Tools
Please Make sure that you have the following tools on your machine:
- node.js 16
- yarn 1.22
- mongodb server (replicat set required)

## Packages installation

Move to the project directory and run:
```
 yarn install
```

## Configuration

You find configurations for each environment under the folder ``config``, some configurations can be passed via environment variables like ``mongoUri``, this is made for example to avoid putting the prod db connection data directly in code, and instead you can use the environment variable ``MONGO_URL``

## Run locally
After installing the required packages, you can run the project locally by executing the following command:
```
 yarn start
```

## Run tests
After installing the required packages, you can run tests locally by executing the following command:
```
 yarn test
```

## Build a prod version
To build a prod version of the app, run the following command:
```
 yarn build
```

## Run the prod version
After building a prod version, run the following command:
```
 yarn start:prod
```
