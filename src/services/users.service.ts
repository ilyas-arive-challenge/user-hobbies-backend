import User from "../models/user.model";

export const addUser = async function (name: string) {
  try {
    
    const user = new User({ name });
    await user.save();
    
    return {
      _id: user._id,
      name: user.name
    };

  } catch (e) {
    console.error(e);
    throw Error((<Error>e).message);
  }
};

export const getUsers = async function (offset = 0, limit = 100) {
  try {
    
    const [users, count] = [
      await User.find().populate('hobbies').skip(offset).limit(limit),
      await User.find().count()
    ];

    return {
      count,
      offset,
      limit,
      data: users
    };

  } catch (e) {
    console.log(e);
    throw Error((<Error>e).message);
  }
};

export const isUserExists = async function (id: string) {
  return await User.exists({ _id: id });
};