import mongoose from "mongoose";
import Hobby from "../models/hobby.model";
import User from "../models/user.model";


export const addHobby = async function (name: string, year: number, passionLevel: string, user: string) {
  const session = await mongoose.startSession();
  try {
    session.startTransaction();
     
    const hobby = new Hobby({ name, year, passionLevel, user });
    await hobby.save({ session });

    await User.updateOne({
      _id: user
    }, {
      $addToSet: { hobbies: hobby.id },
    }, { session });

    await session.commitTransaction();

    return {
      _id: hobby._id,
      name: hobby.name,
      year: hobby.year,
      passionLevel: hobby.passionLevel
    };

  } catch (e) {
    console.error(e);
    throw Error((<Error>e).message);
  } finally {
    await session?.endSession();
  }
};

export const deleteHobby = async function (id: string) {
  const session = await mongoose.startSession();
  try {
    session.startTransaction();
    
    await Hobby.deleteOne({ _id: id }, { session })

    await User.updateOne({
      hobbies: id
    }, {
      $pull: { hobbies: id }
    }, { session });

    await session.commitTransaction();

  } catch (e) {
    console.error(e);
    throw Error((<Error>e).message);
  } finally {
    await session?.endSession();
  }
};

export const isHobbyExists = async function (id: string) {
  return await Hobby.exists({ _id: id });
};