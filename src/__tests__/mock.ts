export const EMPTY_BODY = {};

export const ADD_USER_BODY = {
  name: "Ilyas"
};

export const ADD_HOBBY_BODY = (user: string) => ({
  name: "Football",
  year: 2015,
  passionLevel: "High",
  user
});

export const ADD_HOBBY_WRONG_PASSION_LEVEL_BODY = (user: string) => ({
  name: "Football",
  year: 2015,
  passionLevel: "So Much!",
  user
});

export const ADD_HOBBY_WITHOUT_USER_BODY = {
  name: "Football",
  year: 2015,
  passionLevel: "So Much!"
};