import mongoose from 'mongoose'
import { MongoMemoryReplSet } from 'mongodb-memory-server';

let mongoServer: MongoMemoryReplSet;

export async function Connect() {
   mongoServer = await MongoMemoryReplSet.create();
   const uri = mongoServer.getUri();

   mongoose.connect(uri, err => {
      if (err) {
         console.error(err);
      }
   })
}

export async function CloseDatabase() {
   await mongoose.connection.dropDatabase();
   await mongoose.connection.close();
   await mongoServer.stop();
}

export async function ClearDatabase() {
   const collections = mongoose.connection.collections;

   for (const key in collections) {
      const collection = collections[key];
      await collection.deleteMany({});
   }
}
