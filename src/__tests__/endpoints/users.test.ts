import request from 'supertest';
import app from '../../app';
import { HttpStatusEnum } from "../../shared/enums/httpStatus.enum";
import { ADD_USER_BODY, EMPTY_BODY } from "../mock";
import * as DatabaseHelper from "../db.helper";

describe("users endpoint", () => {

  beforeAll(async () => {
    await DatabaseHelper.Connect();
  });

  afterAll(async () => {
    await DatabaseHelper.ClearDatabase();
    await DatabaseHelper.CloseDatabase();
  });

  test("should create a user", async () => {
    const response = await request(app)
      .post("/api/v1/users")
      .send(ADD_USER_BODY);

    expect(response.status).toBe(HttpStatusEnum.SUCCESS);
    expect(response.body).toHaveProperty('_id');
    expect(response.body).toHaveProperty('name');
    expect(response.body.name).toEqual(ADD_USER_BODY.name);
  });

  test("should fail to create a user without name", async () => {
    const response = await request(app)
      .post("/api/v1/users")
      .send(EMPTY_BODY);

    expect(response.status).toBe(HttpStatusEnum.BAD_REQUEST);
  });
  
  test("should get all users list", async () => {
    const response = await request(app)
      .get("/api/v1/users");

    expect(response.status).toBe(HttpStatusEnum.SUCCESS);
    expect(response.body).toHaveProperty('count');
    expect(response.body).toHaveProperty('offset');
    expect(response.body).toHaveProperty('limit');
    expect(response.body).toHaveProperty('data');
    expect(response.body.data).toBeInstanceOf(Array);
    expect(response.body.data).toHaveLength(1);
  });

});