import request from 'supertest';
import app from '../../app';
import { HttpStatusEnum } from "../../shared/enums/httpStatus.enum";
import {
  ADD_HOBBY_BODY,
  ADD_HOBBY_WRONG_PASSION_LEVEL_BODY,
  ADD_HOBBY_WITHOUT_USER_BODY,
  ADD_USER_BODY
} from "../mock";
import * as DatabaseHelper from "../db.helper";

let userId: string;
let hobbyId: string;

beforeAll(async () => {
  await DatabaseHelper.Connect();
  // Create a user to be able to test hobbies route
  const response = await request(app)
      .post("/api/v1/users")
      .send(ADD_USER_BODY);
  userId = response.body._id;
});

afterAll(async () => {
  await DatabaseHelper.ClearDatabase();
  await DatabaseHelper.CloseDatabase();
});


describe('hobbies endpoint', () => {

  test('should create a hobby', async () => {
    const BODY = ADD_HOBBY_BODY(userId);

    const response = await request(app)
      .post("/api/v1/hobbies")
      .send(BODY);

    expect(response.status).toBe(HttpStatusEnum.SUCCESS);
    expect(response.body).toHaveProperty('_id');
    expect(response.body).toHaveProperty('name');
    expect(response.body).toHaveProperty('year');
    expect(response.body).toHaveProperty('passionLevel');
    expect(response.body.name).toEqual(BODY.name);
    expect(response.body.year).toEqual(BODY.year);
    expect(response.body.passionLevel).toEqual(BODY.passionLevel);

    hobbyId = response.body._id;
  });

  test('should fail to create hobby with a wrong passion level', async () => {
    const response = await request(app)
      .post("/api/v1/hobbies")
      .send(ADD_HOBBY_WRONG_PASSION_LEVEL_BODY(userId));

    expect(response.status).toBe(HttpStatusEnum.BAD_REQUEST);
  });

  test('should fail to create hobby without a userId', async () => {
    const response = await request(app)
      .post("/api/v1/hobbies")
      .send(ADD_HOBBY_WITHOUT_USER_BODY);

    expect(response.status).toBe(HttpStatusEnum.BAD_REQUEST);
  });

  test('should delete a hobby', async () => {
    const response = await request(app)
      .delete("/api/v1/hobbies/" + hobbyId);

    expect(response.status).toBe(HttpStatusEnum.SUCCESS_NO_CONTENT);
  });

  test('should fail to delete a hobby with a wrong id', async () => {
    const response = await request(app)
      .delete("/api/v1/hobbies/blablabla");

    expect(response.status).toBe(HttpStatusEnum.BAD_REQUEST);
  });

});