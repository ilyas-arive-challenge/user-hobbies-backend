import express, { Request, Response } from "express";
import config from "config";
import initRoutes from "./routes";

const app = express();

const port: number = config.has("port") ? config.get<number>("port") : Number(config.util.getEnv("PORT"));

app.set("port", port);
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.get("/", (req: Request, res: Response) => {
  res.status(200).send("Backend is running...");
});

initRoutes(app);

export default app;