import { Router } from "express";
import { addHobbyHandler, deleteHobbyHandler } from "../../controllers/v1/hobbies.controller";
import { validateRequestBody, validateRequestParams } from "../../middlewares/validateRequest";
import { addHobbyBody, deleteHobbyParams } from "../../schemas/hobby.schemas";

const router = Router();

/**
 * @openapi
 * /api/v1/hobbies:
 *  post:
 *     tags:
 *     - Hobbies
 *     summary: Add a new hobby
 *     requestBody:
 *      required: true
 *      content:
 *        application/json:
 *           schema:
 *               type: object
 *               required:
 *                 - name
 *                 - year
 *                 - passionLevel
 *                 - user
 *               properties:
 *                 name:
 *                   type: string
 *                   default: Football
 *                 year:
 *                   type: string
 *                   default: 2021
 *                 passionLevel:
 *                   type: string
 *                   default: Medium
 *                 user:
 *                   type: string
 *                   default: 507f1f77bcf86cd799439011
 *     responses:
 *      200:
 *        description: Success
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                name:
 *                  type: string
 *                year:
 *                  type: number
 *                passionLevel:
 *                  type: string
 *                _id:
 *                  type: string
 *      400:
 *        description: Bad request
 */
router.post(
  '/',
  validateRequestBody(addHobbyBody),
  addHobbyHandler
);

/**
 * @openapi
 * '/api/v1/hobbies/{id}':
 *  delete:
 *     tags:
 *     - Hobbies
 *     summary: Delete a hobby by id
 *     parameters:
 *      - name: id
 *        in: path
 *        description: id of the hobby
 *        required: true
 *     responses:
 *       200:
 *         description: Success
 *         content:
 *          application/json:
 *           schema:
 *              type: object
 *              required:
 *                - name
 *                - year
 *                - passionLevel
 *                - id
 *              properties:
 *                name:
 *                  type: string
 *                  default: Football
 *                year:
 *                  type: number
 *                  default: 2021
 *                passionLevel:
 *                  type: string
 *                  default: High
 *                id:
 *                  type: string
 *       400:
 *         description: Bad request
 */
router.delete(
  '/:id',
  validateRequestParams(deleteHobbyParams),
  deleteHobbyHandler
);

export default router;