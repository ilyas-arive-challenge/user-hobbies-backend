import { Router } from "express";
import { addUserHandler, getUsersHandler } from "../../controllers/v1/users.controller";
import { processRequestQuery, validateRequestBody, validateRequestQuery } from "../../middlewares/validateRequest";
import { addUserBody, getUsersQuery } from "../../schemas/user.schemas";

const router = Router();

/**
 * @openapi
 * /api/v1/users:
 *  post:
 *     tags:
 *     - Users
 *     summary: Add a new user
 *     requestBody:
 *      required: true
 *      content:
 *        application/json:
 *           schema:
 *               type: object
 *               required:
 *                 - name
 *               properties:
 *                 name:
 *                   type: string
 *                   default: Jane Doe
 *     responses:
 *      200:
 *        description: Success
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                name:
 *                  type: string
 *                _id:
 *                  type: string
 *                hobbies:
 *                  type: array
 *      400:
 *        description: Bad request
 */
router.post(
  '/',
  validateRequestBody(addUserBody),
  addUserHandler
);

/**
 * @openapi
 * /api/v1/users:
 *   get:
 *     tags:
 *     - Users
 *     summary: Retrieve list of users
 *     parameters:
 *      - in: query
 *        name: offset
 *        type: integer
 *        description: The number of items to skip before starting to collect the result set
 *      - in: query
 *        name: limit
 *        type: integer
 *        description: The numbers of items to return.
 *     responses:
 *       200:
 *         description: List of users.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 count:
 *                   type: integer
 *                   description: Total number of users
 *                   example: 200
 *                 offset:
 *                   type: integer
 *                   description: Offset value
 *                   example: 10
 *                 limit:
 *                   type: integer
 *                   description: Limit value
 *                   example: 10
 *                 data:
 *                   type: array
 *                   items:
 *                     type: object
 *                     properties:
 *                       _id:
 *                         type: string
 *                         description: The user ID.
 *                         example: 507f1f77bcf86cd799439011
 *                       name:
 *                         type: string
 *                         description: The user's name.
 *                         example: Leanne Graham
 */
router.get(
  '/',
  [
    validateRequestQuery(getUsersQuery),
    processRequestQuery(getUsersQuery)
  ],
  getUsersHandler
);

export default router;