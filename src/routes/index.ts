import { Express } from "express";
import usersRouteV1 from "./v1/users.route";
import hobbiesRouteV1 from "./v1/hobbies.route";

async function initRoutes(app: Express) {
  app.use("/api/v1/users", usersRouteV1);
  app.use("/api/v1/hobbies", hobbiesRouteV1);
}

export default initRoutes;