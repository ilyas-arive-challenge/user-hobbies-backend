import { Types } from "mongoose";
import { number, object, string } from "zod";
import { isHobbyExists } from "../services/hobbies.service";
import { isUserExists } from "../services/users.service";
import { PassionLevels } from "../shared/enums/hobby.enum";
import { ErrorResponse } from "./error.schemas";


export const addHobbyBody = object({
  //name field
  name: string({
    required_error: "name field is required",
    invalid_type_error: "name field type must be a string",
  }),

  //year field
  year: number({
    required_error: "year field is required",
    invalid_type_error: "year field value must be a number between 1900 and 2022",
  }).min(1900, "year field value must be at least 1900")
    .max(2022, "year field value must be at most 2022"),

  //passionLevel field
  passionLevel: string({
    required_error: "passionLevel is required",
  }).refine((value) => Object.values(PassionLevels).includes(value as PassionLevels), {
    message: "passionLevel field value should have one of those values: " + Object.values(PassionLevels).join(),
  }),

  //user field
  user: string({
    required_error: "user field is required",
    invalid_type_error: "user field value must be a string",
    //Check if the provided user is valid
  }).refine(async (userId) => {
    try {
      return await isUserExists(userId);
    } catch (error) {
      return false;
    }

  }, {
    message: "Invalid user field value"
  })

});

export type AddHobbyBody = typeof addHobbyBody;

export interface addHobbyResponse {
  _id: Types.ObjectId;
  name: string;
  passionLevel: string;
  year: number;
}

export type AddHobbyResponse = addHobbyResponse | ErrorResponse;

export const deleteHobbyParams = object({
  //id parameter
  id: string({
    required_error: "id parameter is required",
    invalid_type_error: "id parameter value must be a string",
    //Check if the provided id is valid
  }).refine(async (hobbyId) => {
    try {
      return await isHobbyExists(hobbyId);
    } catch (error) {
      return false;
    }

  }, {
    message: "Invalid id parameter value"
  })
});

export type DeleteHobbyParams = typeof deleteHobbyParams;

export type DeleteHobbyResponse = ErrorResponse;