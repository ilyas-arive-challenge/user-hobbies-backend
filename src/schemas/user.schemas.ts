import { Types } from "mongoose";
import { number, object, preprocess, string } from "zod";
import { stringToNumber } from "../utils/types";
import { ErrorResponse } from "./error.schemas";

export const addUserBody = object({
  //name field
  name: string({
    required_error: "name field is required",
    invalid_type_error: "name field type must be a string",
  })
});

export type AddUserBody = typeof addUserBody;

export interface addUserResponse {
  _id: Types.ObjectId;
  name: string;
}

export type AddUserResponse = addUserResponse | ErrorResponse;

export const getUsersQuery = object({
  //offset field
  offset: preprocess(stringToNumber,
    number({
      invalid_type_error: "offset value must be a positive number",
    })
      .nonnegative()
      .optional()),

  //limit field
  limit: preprocess(stringToNumber,
    number({
      invalid_type_error: "limit value must be a positive number",
    })
      .nonnegative()
      .optional())
   
  // query params are passed as string
  // transform will convert them to numbers
}).transform(query => {
  return object({
    offset: number().optional(),
    limit: number().optional()
  }).parse({
    offset: stringToNumber(query.offset),
    limit: stringToNumber(query.limit)
  });
});

export type GetUsersQuery = typeof getUsersQuery;

export interface getUsersResponse {
  count: number,
  offset: number,
  limit: number,
  data: any[]
}

export type GetUsersResponse = getUsersResponse | ErrorResponse;