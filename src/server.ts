import app from "./app";
import { connectToDatabase } from "./utils/database";
import swaggerDocs from "./utils/swagger";

const port: number = app.get("port");

const server = app.listen(port, async () => {
  console.log(`App is running http://localhost:${port}`);
  if (process.env.NODE_ENV !== "test") {
    await connectToDatabase();
  }
  swaggerDocs(app, port);
});

export default server;