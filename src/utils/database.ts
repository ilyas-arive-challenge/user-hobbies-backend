import mongoose from "mongoose";
import config from "config";

export async function connectToDatabase() {
  const mongoUri = config.has("mongoUri") ? config.get<string>("mongoUri") : config.util.getEnv("MONGO_URL");
  try {
    await mongoose.connect(mongoUri);
    console.log('Connected to MongoDB Server');
  } catch (error) {
    console.error(error);
    process.exit(1);
  }
}

export async function disconnectFromDatabase() {
  try {
    await mongoose.disconnect();
  } catch (error) {
    console.error(error);
  }
}