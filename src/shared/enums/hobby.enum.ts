export enum PassionLevels {
  Low = "Low",
  Medium = "Medium",
  High = "High",
  VeryHigh = "Very-High"
}