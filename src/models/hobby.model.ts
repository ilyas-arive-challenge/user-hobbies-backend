import { Schema, model, Types } from "mongoose";
import { PassionLevels } from "../shared/enums/hobby.enum";

interface Hobby {
  name: string;
  passionLevel: string;
  year: number;
  user: Types.ObjectId;
}

const hobbySchema = new Schema<Hobby>({
  name: {
    type: String,
    required: true
  },
  passionLevel: {
    type: String,
    required: true,
    enum: PassionLevels
  },
  year: {
    type: Number,
    required: true,
    min: 1900,
    max: 2022
  },
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
  }
}, {
  versionKey: false
});

const Hobby = model<Hobby>('Hobby', hobbySchema);

export default Hobby;