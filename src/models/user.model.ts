import { Schema, model, Types } from "mongoose";

interface User {
  name: string;
  hobbies: Array<Types.ObjectId>;
}

const userSchema = new Schema<User>({
  name: {
    type: String,
    required: true
  },
  hobbies: [{
    type: Schema.Types.ObjectId,
    ref: 'Hobby'
  }]
}, {
  versionKey: false
});

const User = model<User>('User', userSchema);

export default User;