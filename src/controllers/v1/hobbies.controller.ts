import { Response } from "express";
import { TypedRequestBody, TypedRequestParams } from "../../middlewares/validateRequest";
import { AddHobbyBody, AddHobbyResponse, DeleteHobbyParams, DeleteHobbyResponse } from "../../schemas/hobby.schemas";
import { addHobby, deleteHobby } from "../../services/hobbies.service";
import { HttpStatusEnum } from "../../shared/enums/httpStatus.enum";

export const addHobbyHandler = async (req: TypedRequestBody<AddHobbyBody>, res: Response<AddHobbyResponse>) => {
  try {
    const { name, year, passionLevel, user } = req.body;
    const hobby = await addHobby(name, year, passionLevel, user);
    res.status(HttpStatusEnum.SUCCESS).json(hobby);
  } catch (e) {
    console.error(e);
    res.status(HttpStatusEnum.SERVER_ERROR).json({ message: (<Error>e).message  });
  }
}

export const deleteHobbyHandler = async (req: TypedRequestParams<DeleteHobbyParams>, res: Response<DeleteHobbyResponse>) => {
  try {
    const { id } = req.params;
    await deleteHobby(id);
    res.status(HttpStatusEnum.SUCCESS_NO_CONTENT).send();
  } catch (e) {
    console.error(e);
    res.status(HttpStatusEnum.SERVER_ERROR).json({ message: (<Error>e).message  });
  }
}