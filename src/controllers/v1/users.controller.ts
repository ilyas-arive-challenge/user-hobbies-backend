import { Response } from "express";
import { TypedRequestBody, TypedRequestQuery } from "../../middlewares/validateRequest";
import { addUser, getUsers } from "../../services/users.service";
import { AddUserBody, AddUserResponse, GetUsersQuery, GetUsersResponse } from '../../schemas/user.schemas';
import { HttpStatusEnum } from "../../shared/enums/httpStatus.enum";

export const addUserHandler = async (req: TypedRequestBody<AddUserBody>, res: Response<AddUserResponse>) => {
  try {
    const { name } = req.body;
    const user = await addUser(name);
    res.status(HttpStatusEnum.SUCCESS).json(user);
  } catch (e) {
    console.error(e);
    res.status(HttpStatusEnum.SERVER_ERROR).json({ message: (<Error>e).message });
  }
}

export const getUsersHandler = async (req: TypedRequestQuery<GetUsersQuery>, res: Response<GetUsersResponse>) => {
  try {
    const { offset, limit } = req.query;
    const users = await getUsers(offset, limit);
    res.status(HttpStatusEnum.SUCCESS).json(users);
  } catch (e) {
    console.error(e);
    res.status(HttpStatusEnum.SERVER_ERROR).json({ message: (<Error>e).message  });
  }
}